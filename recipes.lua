
-- # Default minetest game recipes

-- ## Tree

horadric_cube:register_craft({
  input = "default:acacia_tree",
  output = "default:aspen_tree",
})

horadric_cube:register_craft({
  input = "default:aspen_tree",
  output = "default:jungletree",
})

horadric_cube:register_craft({
  input = "default:jungletree",
  output = "default:pine_tree",
})

horadric_cube:register_craft({
  input = "default:pine_tree",
  output = "default:tree",
})

horadric_cube:register_craft({
  input = "default:tree",
  output = "default:acacia_tree",
})


-- ## Wood

horadric_cube:register_craft({
  input = "default:acacia_wood 4",
  output = "default:acacia_tree",
})

horadric_cube:register_craft({
  input = "default:aspen_wood 4",
  output = "default:aspen_tree",
})

horadric_cube:register_craft({
  input = "default:junglewood 4",
  output = "default:jungletree",
})

horadric_cube:register_craft({
  input = "default:pine_wood 4",
  output = "default:pine_tree",
})

horadric_cube:register_craft({
  input = "default:wood 4",
  output = "default:tree",
})



-- ## Sand

horadric_cube:register_craft({
  hidden = false,
  input = "default:sand",
  output = "default:desert_sand"
})

horadric_cube:register_craft({
  hidden = false,
  input = "default:desert_sand",
  output = "default:silver_sand"
})

horadric_cube:register_craft({
  hidden = false,
  input = "default:silver_sand",
  output = "default:sand"
})


-- ## Other

horadric_cube:register_craft({
  hidden = true,
  input = "default:coal_lump 9",
  output = "default:diamond"
})

horadric_cube:register_craft({
  hidden = false,
  input = "default:tree",
  output = "default:coal_lump"
})
